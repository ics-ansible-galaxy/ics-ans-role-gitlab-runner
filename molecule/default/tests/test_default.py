import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_gitlab_runner_installed(host):
    assert host.package("gitlab-runner").is_installed


def test_vm_max_map_count(host):
    with host.sudo():
        assert host.sysctl("vm.max_map_count") == 262144


def test_config_toml(host):
    with host.sudo():
        assert host.file("/etc/gitlab-runner/config.toml").contains("concurrent = 1")


def test_git2u_not_installed(host):
    assert not host.package("git2u-core").is_installed


def test_git_version(host):
    command_output = host.check_output("git --version")
    git_version = command_output.split()[2]
    git_major_version = git_version.split('.')[0]
    assert git_major_version == "2"
