ics-ans-role-gitlab-runner
==========================

Ansible role to install gitlab-runner.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
gitlab_runner_version: 14.1.0-1
# List of runners to register
# gitlab_runner_to_register:
#   - name: myrunner1
#     tags: tag1,tag2
#     executor: docker
#     extra_options: "--docker-volumes /var/run/docker.sock:/var/run/docker.sock"
#   - name: myrunner2
#     tags: tag3
#     executor: shell
gitlab_runner_to_register: []
gitlab_runner_registration_token: token
gitlab_runner_url: https://mygitlab.example.org
# Maximum build log size in kilobytes, by default set to 6144 (6MB)
gitlab_runner_output_limit: 6144
# Limits how many jobs globally can be run concurrently
gitlab_runner_concurrent_jobs: 1
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitlab-runner
```

License
-------

BSD 2-clause
